MPEG-TS parsers
===============

MPEG-TS, h264, h265, AV1 parsing library. Zero dependencies.

Supports rudimentary stream analysis of mpeg-ts streams.

