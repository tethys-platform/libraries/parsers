use log::debug;

#[derive(Debug)]
pub struct NalAnalysisResult {
    pub i_frame: bool,
    pub nonref_frame: bool,
    pub found_vps: bool,
    pub found_sps: bool,
    pub found_pps: bool,
}

pub trait VideoNalParser {
    fn push(&mut self, data: &[u8]);
    fn analyze(&mut self) -> NalAnalysisResult;
}

impl VideoNalParser for crate::h264::H264NalParser {
    fn push(&mut self, data: &[u8]) {
        self.push(data);
    }

    fn analyze(&mut self) -> NalAnalysisResult {
        use crate::h264::NALUType;

        let mut found_frame = false;
        let mut nonref_frame = false;
        let mut found_sps = false;
        let mut found_pps = false;
        let mut i_frame = false;

        for (nal_type, slice_type) in self {

            if found_frame && found_sps && found_pps {
                break;
            }

            match nal_type {
                NALUType::CodedSliceOfIDRPicture => {
                    debug!("Found IDR, st: {:?}", slice_type); 
                    found_frame = true;
                    i_frame = true;
                },
                NALUType::CodedSliceOfNonIDRPicture(non_ref) => {
                    nonref_frame = non_ref;
                    found_frame = true;
                },
                NALUType::SequenceParameterSet => {
                    found_sps = true;
                },
                NALUType::PictureParameterSet => {
                    found_pps = true;
                },
                _ => (),
            }
        }

        NalAnalysisResult { i_frame, found_vps: true, found_sps, found_pps, nonref_frame }
    }
}
 
impl VideoNalParser for crate::h265::H265NalParser {
    fn push(&mut self, data: &[u8]) {
        self.push(data);
    }

    fn analyze(&mut self) -> NalAnalysisResult {
        use crate::h265::NALUType;

        let mut found_frame = false;
        let nonref_frame = false; //TODO: support h265 non-ref-frames
        let mut found_vps = false;
        let mut found_sps = false;
        let mut found_pps = false;
        let mut i_frame = false;

        for nal_type in self {

            if found_frame && found_vps && found_sps && found_pps {
                break;
            }

            debug!("hevc NAL: {:?}", nal_type);

            match nal_type {
                NALUType::CodedSliceSegmentOfIDRPicture | NALUType::CodedSliceSegmentOfCRAPicture => {
                    found_frame = true;
                    i_frame = true;
                },
                //NALUType::CodedSliceOfNonIDRPicture => {
                //    found_frame = true;
                //},
                NALUType::VideoParameterSet => {
                    found_vps = true;
                },
                NALUType::SequenceParameterSet => {
                    found_sps = true;
                },
                NALUType::PictureParameterSet => {
                    found_pps = true;
                },
                _ => (),
            }
        }

        NalAnalysisResult { i_frame, found_vps, found_sps, found_pps, nonref_frame }
    }
}

impl VideoNalParser for crate::av1::AV1OBUParser {
    fn push(&mut self, data: &[u8]) {
        self.push(data);
    }

    fn analyze(&mut self) -> NalAnalysisResult {
        use crate::av1::OBUType;
        use crate::av1::FrameType;

        let mut found_frame = false;
        let nonref_frame = false; //TODO: support h265 non-ref-frames
        let mut found_vps = false;
        let mut found_sps = false;
        let mut found_pps = false;
        let mut i_frame = false;

        for (obu_type, frame_type) in self {

            if found_frame && found_vps && found_sps && found_pps {
                break;
            }

            match frame_type {
                Some(FrameType::KeyFrame) => {
                    found_frame = true;
                    i_frame = true;
                },
                Some(FrameType::InterFrame) | Some(FrameType::IntraOnlyFrame) => {
                    found_frame = true;
                },
                _ => {
                },
            }

            match obu_type {
                OBUType::SequenceHeader => {
                    found_sps = true;
                    found_vps = true;
                    found_pps = true;
                },
                _ => (),
            }
        }

        NalAnalysisResult { i_frame, found_vps, found_sps, found_pps, nonref_frame }
    }
}


