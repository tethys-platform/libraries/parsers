pub mod h264;
pub mod h265;
pub mod av1;
pub mod mpegts;
pub mod pes;
pub mod video_nal_parser;


#[cfg(test)]
mod tests {
    use hex_literal::*;

    const H264_PAYLOAD: [u8; 1140] = hex!("
        000000010930000000000001274d40288d9500f0044fcb808800001f4800
        075304200000000128fe1e200000000125888000003890081c016783ff1c
        607eae878852663b6572cf212a8b31139634c245db702f8338ec1d8854fe
        fb09a217869deb5bb2a3b8b21e41c5f08281a26e22086db52bf96b6324c2
        28ab0c3408cd519df1f0dbbf59b6ec813a2c4f7dd922ec7494b2c241ed19
        56ae512145dd0a022706b2f9dc0eaef0dca33054a7ca30f24be5df7ffd01
        b10c96778dfc186c864cf9c5c92d5a5762a7c63c70109223be758c11da12
        985f08cea9bac14e53c88a8f0c7c51a823efba147a857486c33b37522fb8
        eb17b74369f2fd9bf1348e5fdd614cd2372cc4e89a23707895ee3d1c983d
        9944835eccef1b77e3e1a76b9166523ec0b2c5baf3e0eee99959625b624a
        2d56318630811078165f517e1c53632681b588d1602faf1d4a9c4519eb38
        badbcfcdcb2145f898d09fe9e1ee7ec581d88518454fc07aff40d86a2211
        a7d89e2e11a8d1d1160dad5ec26d64cafd2f6f1d065c216147ab5c599a1b
        6ec61cf8aa92c3cf676c7b9744ecdea5460da61f39f619a92f915304f314
        cae9f23274e700a9b7bcfabe7f754d93fcf0c53f7056f236ef29625d9fd7
        ddbc48a27adb3f5dd93f8cdc2f1eefb0d178aa4d52ec164a698abf6d67b1
        53d6388b427a10637a9522e9c3b184944259b07523603f57f5ea2a5f91dc
        dd6d244c37881be9238fef06dea9e52e2574ed50c8f0ed75ca49c553f514
        f65997c6976dea672c3b466e6f7bd008a9fd15134155f8808932a9cad5af
        4b23e135e5b475975c796012b6d9614f35ee863fc388897cd194b63c06eb
        09a3fb5fa2ce69a86c97f01e30c15077d719207a78c2762d0c2528b751eb
        472704beeea09a721052d399f3e248e99776a091b4aa109c30bb4c3bb42a
        0afb8c5002a5d5344c3177f1b54bc384d7c3936c86efb36b2159331f11e7
        58813c09cb77e3b470f9c37b2208f4a29f2d22224d70053da98e81564d2e
        f2dbbd50aec56599360ee0a69b5f9e64b4c320ccf6c694d3bb797373f1d9
        8d298e0ea4ef6123f26321a05468a79e1d1554afb5905001777dab9dd38f
        56727aaa0696c5e98f0aee43c79cb8018293906c3c8598f2825b2b2bf8b6
        b9c779e7d8ee5f5e004886cddc463262f5871c2715ac86080b008609b5a2
        d5ebfe015ac90ba78b5f631720cd8382fc992393c110a4e0d03e42f44f2a
        f4b2ce04af7c6add4c3bfd13e65423c77859ee3ad8a4b7486e5ebf12b7ef
        f6b73c49d6afe6bac23a94eb1783f5053c9874c715cf1ceeaf7284ededdf
        8e661ced1a526086d84f64da8835e9d86e6c72755975690ba80449c29134
        90e7d4fd813c4d4a5213593b3344a4236d0f79f25da9889b9c274d500a13
        c7eadfc7a32a7bb3a63d2b228fb9f39e801d8470206889e3b231276574d4
        a25f66c65aebae2b1d6432cd98f939e3ef90dbcd43dc49a4d97730ea4ca7
        faab47424d5218db4a9008585657bfa42907d87514a93e8eefe2da02e30e
        edcb8e74d69c934c509d80710828ed04a7d4e2d7a81e4c8bf73f02d4a029
        d4dc1172c4bebecbfe77eb46830f5ad4e369b64870689c01aa540f8daea2
        ");

    #[test]
    fn test_h264() {
        use super::*;
        let mut parser = h264::H264NalParser::new();

        // testing the VideoNalParser iface
        parser.push(&H264_PAYLOAD);
        use video_nal_parser::VideoNalParser;
        let an_res = parser.analyze();
        assert!(an_res.i_frame);
        assert!(an_res.found_vps);
        assert!(an_res.found_sps);
        assert!(an_res.found_pps);

        // again for the iterator
        parser.push(&H264_PAYLOAD);

        let nal_units: Vec<_> = parser.collect();
        assert_eq!(nal_units, vec![
            (h264::NALUType::AccessUnitDelimiter, None), 
            (h264::NALUType::SequenceParameterSet, None),
            (h264::NALUType::PictureParameterSet, None),
            (h264::NALUType::CodedSliceOfIDRPicture, Some(h264::SliceType::ISliceType))]);
    }

    const H265_PAYLOAD: [u8; 1140] = hex!("
        000000014e01010300000302800000000140010c01ffff01400000030000
        03000003000003005aac0900000001420101014000000300000300000300
        0003005aa00a080407c4e5aee42592e30101000003000100000300196005
        de510000927c00003d0901000000014401c0f7c0cc90000000012601af0d
        e033c6fdd6e982773a7aeafcb2905dbccec0d93e7ca0f40474546e4d31b6
        e57cce5326879e4afd915f0ffff8701de091685f377af3b61ff7195c81ff
        eec9707e4df99b19c22fd491e62a8399f4c5655bf84e61cbc322d9311856
        5adef8249729ef322586df249a16dd61ddef9f999abcba8098bdfe1bc9ab
        283c839472daede28e78ea7bc316e9052373669be0fe0671b094159c60b9
        96a0c5011d999a72d37fde36e948420669747084662d07bc357514feb3ed
        5a0daf1cccb0cfecdacff28e7d481cbc1fc6a113660ae7fd70e0b62843ca
        0058c9424ee1b80c8fb6033142766b7d65ef4955f32ba1030eee2deb9a0a
        11655531f5ca87da25419ce4ea6d7af7c5d7c542977060f519013c04fc45
        9ce730d0925fc110db053779af3646c5a8e1412f9536eedbd55d9c02f3dc
        cda7e8350666a67edcdfa0efb38bde0c758a2d06776a6e085cfec1967680
        b076d818e3d8a29ab7d7da62f5e5604d2d1e22cc02db25869069c9bc99c7
        97224b55e109845310a5084c4f6c8fa858221a326a76bd3e1c08819596f1
        9f376f9c46cc46c92e02be1e50c9fb8def44c2d1035d186830320f29fa1c
        9ba6096f3fe974f24d566ead9caaa18180520c4be2c728aa48059b2b9a29
        2364c1987c44c6cd5a99e5a5d693f7fe883a4400cd2a164f317af016b780
        9c1cf331f2bc9a654d4bae5881edac7c2188594b5d600e3ff1f39a2d10c4
        d9436721fb8fb868d166bddb07aedc9779eaffe9474b8350d27e86f39a26
        38521635f82b0a6c219d2983bc56940088ae2988c332617d31008db47c74
        383e768cdda778327c8fb5280475abae1aa15a9f249aa73d1835fc3a7c48
        e174d689173c53264e1582c9f71b44f2722042bb40351d53852d6924544c
        92a868536df5391b6706b22dafe459abb80cb134d0ff77c63f90d510bce9
        01fa56458826256a5c5772eb7e74f098bde2f65f0a3c2817862206bb5f72
        e07d66c1038c8d0c7ed723b6ccf55e08265a8911f2d417cd2f11d4763434
        9a7db0db1cb2bdb7801e69ceae78854b27d3480295244e176adc59989629
        5852c9469b851d50ceb8b3c0da0d3c07f6dfa4ee77633038dd0aa5a49cf7
        e3a11e80048a777fe694179522287edc97f7389b0d5b34d4ec3395627e0b
        d24069388c2e62c3d3e91efd86010c3dde26755c350964c829699014be76
        18463a81bde88343c9ca533f291c009d577eb09f0693e53ced507bd12c05
        80eac96980d19e77d60f95eab3a6e61b7a3fe2282192bf00db6322db2d7e
        396a25e40ff9b26e21bc722e8c0559e9993a60a488e034f7e7c17ebb2d1f
        82478d5e1eb448a4d4de953b308ebe10c6db0697e3f633aaa7dafe250a69
        c8ad333ae3ee4e8782ab5c7b2e63073e22548dc8fac781c2e0bf57327b46
        ec0c1fc70ae631942df5afa9e8f4ca6a2aa29a50896a457240a26f3747fe");

    #[test]
    fn test_h265() {
        use super::*;
        let mut parser = h265::H265NalParser::new();
        parser.push(&H265_PAYLOAD);

        // testing the VideoNalParser iface
        use video_nal_parser::VideoNalParser;
        let an_res = parser.analyze();
        assert!(an_res.i_frame);
        assert!(an_res.found_vps);
        assert!(an_res.found_sps);
        assert!(an_res.found_pps);

        // again for the iterator
        parser.push(&H265_PAYLOAD);

        let nal_units: Vec<_> = parser.collect();
        assert_eq!(nal_units, vec![
            h265::NALUType::SupplementalEnhancementInformation, 
            h265::NALUType::VideoParameterSet, 
            h265::NALUType::SequenceParameterSet, 
            h265::NALUType::PictureParameterSet, 
            h265::NALUType::CodedSliceSegmentOfIDRPicture]);
    }

}
