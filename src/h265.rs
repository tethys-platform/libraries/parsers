#[derive(Debug, PartialEq, PartialOrd)]
pub enum NALUType {
    CodedSliceSegmentOfNonTSANonSTSATrailingPicture,
    CodedSliceSegmentOfTSAPicture,
    CodedSliceSegmentOfSTSAPicture,
    CodedSliceSegmentOfRADLPicture,
    CodedSliceSegmentOfRASLPicture,
    ReservedNonIRAP,
    CodedSliceSegmentOfBLAPicture,
    CodedSliceSegmentOfIDRPicture,
    CodedSliceSegmentOfCRAPicture,
    ReservedIRAP,
    ReservedNonIRAP2,
    VideoParameterSet,
    SequenceParameterSet,
    PictureParameterSet,
    AccessUnitDelimiter,
    EndOfSequence,
    EndOfStream,
    FillerData,
    SupplementalEnhancementInformation,
    Unspecified,
    Reserved,
}

impl NALUType {
    fn from_u8(b: u8) -> NALUType {
        match  (b & 0x7e) >> 1 {
            0 | 1 => NALUType::CodedSliceSegmentOfNonTSANonSTSATrailingPicture,
            2 | 3 => NALUType::CodedSliceSegmentOfTSAPicture,
            4 | 5 => NALUType::CodedSliceSegmentOfSTSAPicture,
            6 | 7 => NALUType::CodedSliceSegmentOfRADLPicture,
            8 | 9 => NALUType::CodedSliceSegmentOfRASLPicture,
            10 | 12 | 14 => NALUType::ReservedNonIRAP,
            11 | 13 | 15 => NALUType::ReservedIRAP,
            16 | 17 | 18 => NALUType::CodedSliceSegmentOfBLAPicture,
            19 | 20  => NALUType::CodedSliceSegmentOfIDRPicture,
            21 => NALUType::CodedSliceSegmentOfCRAPicture,
            22 | 23 | 24 | 25 | 26 | 27 | 28 | 29 | 30 | 31 => NALUType::ReservedNonIRAP2,
            32 => NALUType::VideoParameterSet,
            33 => NALUType::SequenceParameterSet,
            34 => NALUType::PictureParameterSet,
            35 => NALUType::AccessUnitDelimiter,
            36 => NALUType::EndOfSequence,
            37 => NALUType::EndOfStream,
            38 => NALUType::FillerData,
            39 | 40 => NALUType::SupplementalEnhancementInformation,
            41 | 42 | 43 | 44 | 45 | 46 | 47 => NALUType::Unspecified,
            _ => NALUType::Reserved,
        }
    }
}

#[derive(Debug)]
enum H265ParserState {
    Init,
    OneZero,
    TwoZeros,
    StartCode,
}

// Creates an iterator over NAL units
pub struct H265NalParser {
    data: Vec<u8>,
    read_offset: usize,
    state: H265ParserState, 
}

impl H265NalParser {
    pub fn new() -> Self {
        H265NalParser {
            data: Vec::new(),
            read_offset: 0,
            state: H265ParserState::Init,
        }
    }

    // adds new data to the parser
    pub fn push(&mut self, data: &[u8]) {
        self.data.extend_from_slice(data);
    }

    fn to(&mut self, state: H265ParserState) {
        self.state = state;
    }
}

impl Iterator for &mut H265NalParser {
    type Item = NALUType; // start and end offset within self.data

    fn next(&mut self) -> Option<NALUType> {

        while self.read_offset+2 < self.data.len() {
            let b = self.data[self.read_offset];

            match (&self.state, b) {
                (H265ParserState::Init, 0) => self.to(H265ParserState::OneZero),
                (H265ParserState::OneZero, 0) => self.to(H265ParserState::TwoZeros),
                (H265ParserState::TwoZeros, 0) => self.to(H265ParserState::TwoZeros),
                (H265ParserState::TwoZeros, 1) => self.to(H265ParserState::StartCode),
                (H265ParserState::StartCode, b) => { 
                    let t = NALUType::from_u8(b);
                    self.read_offset += 1; // 2B NAL header
                    self.to(H265ParserState::Init);
                    return Some(t);
                },
                (_, _) => {
                    self.to(H265ParserState::Init);
                },
            }

            self.read_offset += 1;
        }

        self.data.clear();
        self.read_offset = 0;
        None
    }
}
