
#[derive(Debug)]
pub struct Pes {
   pub data_length: usize,
   pub data_offset: usize,
   pub pts: Option<u64>,
   pub dts: Option<u64>,
}

#[derive(Debug)]
pub enum PesParseError {
    BadStartCode,
    BadSize,
}

// PES Header
// | Field name   | Size in bits | Value
// | Start prefix | 24           | 0x000001
// | Stream ID    | 8            | Audio streams (0xC0-0xDF), Video streams (0xE0-0xEF)
// | PES pkt len  | 16           |
// | Optional PES | variable     |
// | Data         | variable     |
//
// Optional PES Header
// | Marker bits  | 2            | 0b10
// | Scrambl. ctrl| 2            | 0b00 - means not scrambled
// | Priority     | 1            |
// | Data align   | 1            | 1 indicates that the PES header is immediately followed by video
// |              |              | start code or audio syncword
// | Copyright    | 1            | 1 - copyrighted
// | Orig. or copy| 1            | 1 - original
// | PTSDTS       | 2            | 11 - both, 10 - pts only, 00 - no pts or dts
// | ESCR flag    | 1            |
// | ES rate flag | 1            |
// | DSM trick md | 1            |
// | Additional   | 1            |
// | Copy         |              |
// | Info flag    |              |
// | CRC flag     | 1            |
// | Extension    | 1            |
// | PES hdr len  | 8            | length of the remainder in bytes
// | Optional flds| variable     |
// | Stuffing     | variable     | 0xff
//
pub fn parse_header(data: &[u8]) -> Result<Pes, PesParseError> {
    if data.len() < 9 {
        return Err(PesParseError::BadSize);
    }

    if data[0..3] != [0, 0, 1] {
        return Err(PesParseError::BadStartCode);
    }

    // data[3] -- SID
    let mut data_length = u16::from_be_bytes([data[4], data[5]]) as usize;
    let pts_dts = data[7] & 0b11000000;
    let pes_header_length = data[8];
    let data_offset = pes_header_length as usize + 9;
    let mut pts = None;
    let mut dts = None;

    if data_length > data_offset + 6 {
        data_length = data_length - data_offset + 6;
    }

    match pts_dts {
        0b1000_0000 => {  // pts only
            if data.len() > 14 {
                pts = parse_ts(&data[9..14], 0b0010_0000).ok();
            }
        }
        0b1100_0000 => {  // pts and dts
            if data.len() > 14 {
                pts = parse_ts(&data[9..14], 0b0011_0000).ok();
            }

            if data.len() > 19 {
                dts = parse_ts(&data[14..19], 0b0001_0000).ok();
            }
        }
        _ => {
        }
    }

    Ok(Pes { data_length, data_offset, pts, dts })
}

fn parse_ts(bytes: &[u8], _prefix_nibble_high: u8) -> Result<u64, PesParseError> {
    if bytes.len() != 5 {
        return Err(PesParseError::BadSize);
    }

    //if bytes[0] & 0b1111_0000 != prefix_nibble_high {
    //    return Err(PesParseError::BadStartCode);
    //}

    let mut ts = 0u64;
    ts |= ((bytes[0] & 0b0000_1110) as u64) << 29;
    ts |= (bytes[1] as u64) << 22;
    ts |= ((bytes[2] & 0b1111_1110) as u64) << 14;
    ts |= (bytes[3] as u64) << 7;
    ts |= (bytes[4] as u64) >> 1;

    Ok(ts)
}

use crate::mpegts::{SidType};

pub fn extract_video_pes(frame: &[u8]) -> (Vec<u8>, i64, i64) {
    extract_single_pes(frame, SidType::Video)
}

pub fn extract_klv_pes(frame: &[u8]) -> (Vec<u8>, i64, i64) {
    extract_single_pes(frame, SidType::Klv)
}

pub fn extract_single_pes(frame: &[u8], sid_type: SidType) -> (Vec<u8>, i64, i64) {
    use crate::mpegts::Parser;
    let mut parser = Parser::new();
    let mut out_pts = -1i64;
    let mut out_dts = -1i64;
    let mut out_data = vec![];

    use crate::mpegts::{Payload, TsPacket};
    let mut res_pid = None;

    for chunk in frame.chunks_exact(188) {
        match parser.parse(chunk) {
            Ok(TsPacket {
                pid,
                cc: _,
                adaptation: _,
                payload: Some(Payload::Pes(pes)),
            }) if pes.sid_type == sid_type => {
                out_data.extend_from_slice(&pes.data[pes.data_offset..]);
                res_pid = Some(pid);
                if let Some(pts) = pes.pts {
                    out_pts = pts as i64;
                }
                if let Some(dts) = pes.dts {
                    out_dts = dts as i64;
                }
            }
            Ok(TsPacket {
                pid,
                cc: _,
                adaptation: _,
                payload: Some(Payload::Data(data)),
            }) => {
                if res_pid.is_some() && pid == res_pid.unwrap() {
                    out_data.extend_from_slice(data);
                }
            }
            _ => {}
        }
    }

    if out_dts == -1 {
        out_dts = out_pts;
    }

    (out_data, out_pts, out_dts)
}

pub fn extract_pes(frame: &[u8]) -> Vec<(SidType, u16, Vec<u8>, i64, i64)> {
    use crate::mpegts::Parser;
    let mut parser = Parser::new();

    // small num. of elements, vec is more efficient than hashmap
    let mut res : Vec<(SidType, u16, Vec<u8>, i64, i64)> = vec![];

    use crate::mpegts::{Payload, TsPacket};

    for chunk in frame.chunks_exact(188) {
        match parser.parse(chunk) {
            Ok(TsPacket {
                pid,
                cc: _,
                adaptation: _,
                payload: Some(Payload::Pes(pes)),
            }) => {
                let mut out_data = vec![];
                out_data.extend_from_slice(&pes.data[pes.data_offset..]);

                let mut out_pts = -1i64;
                let mut out_dts = -1i64;

                if pes.pts.is_some() {
                    out_pts = pes.pts.unwrap() as i64;
                }

                if pes.dts.is_some() {
                    out_dts = pes.dts.unwrap() as i64;
                }

                if let Some((_, _, res_data, _, _)) = res.iter_mut().find(|(res_sid_type, res_pid, _, _, _)|{
                    pes.sid_type == *res_sid_type && pid == *res_pid
                }) {
                    res_data.extend_from_slice(&out_data);
                } else {
                    res.push((pes.sid_type, pid, out_data, out_pts, out_dts));
                }
            }
            Ok(TsPacket {
                pid,
                cc: _,
                adaptation: _,
                payload: Some(Payload::Data(data)),
            }) => {
                if let Some((_, _, out_data, _, _)) = res.iter_mut().find(|(_, res_pid, _, _, _)|{
                    pid == *res_pid
                }) {
                    out_data.extend_from_slice(data);
                }
            }
            _ => {}
        }
    }

    for (_, _, _, out_pts, out_dts) in res.iter_mut() {
        *out_dts = *out_pts;
    }

    res
}

#[test]
fn pes1() {
    use hex_literal::*;
    const PES_HEADER: [u8; 64] =
        hex!("0000 01fc 0195 8480 0521 0007 d861 060e
                  2b34 020b 0101 0e01 0301 0100 0000 81bd
                  0208 0005 bd23 9fa6 95e8 030a 5541 5656
                  5f43 4d32 3632 0502 bef9 0602 0a8b 0702");

    println!("{:?}", parse_header(&PES_HEADER));
}
