#[derive(Debug, PartialEq, PartialOrd)]
pub enum NALUType {
    Unspecified,
    CodedSliceOfNonIDRPicture(bool),
    CodedSliceDataPartitionA,
    CodedSliceDataPartitionB,
    CodedSliceDataPartitionC,
    CodedSliceOfIDRPicture,
    SEI,
    SequenceParameterSet,
    PictureParameterSet,
    AccessUnitDelimiter,
    EndOfSequence,
    EndOfStream,
    FillerData,
    Error,
}

impl NALUType {
    fn from_u8(b: u8) -> NALUType {

        let non_ref_pic = ((b & 0b_0110_0000) >> 5)  <= 1;

        match  b & 0b0001_1111 {
            0 => NALUType::Unspecified,
            1 => NALUType::CodedSliceOfNonIDRPicture(non_ref_pic),
            2 => NALUType::CodedSliceDataPartitionA,
            3 => NALUType::CodedSliceDataPartitionB,
            4 => NALUType::CodedSliceDataPartitionC,
            5 => NALUType::CodedSliceOfIDRPicture,
            6 => NALUType::SEI,
            7 => NALUType::SequenceParameterSet,
            8 => NALUType::PictureParameterSet,
            9 => NALUType::AccessUnitDelimiter,
            10 => NALUType::EndOfSequence,
            11 => NALUType::EndOfStream,
            12 => NALUType::FillerData,
            _ => NALUType::Error,
        }
    }

    fn is_vcl(&self) -> bool {
        *self > NALUType::Unspecified && *self <= NALUType::CodedSliceOfIDRPicture
    }
}

#[derive(Debug,PartialEq)]
pub enum SliceType {
    PSliceType,
    BSliceType,
    ISliceType,
    SPSliceType,
    SISliceType,
    None,
}

impl SliceType {
    fn from_u8(nr: u8) -> SliceType {
        match nr {
            0 => SliceType::PSliceType,
            1 => SliceType::BSliceType,
            2 => SliceType::ISliceType,
            3 => SliceType::SPSliceType,
            4 => SliceType::SISliceType,
            _ => SliceType::None,
        }
    }
}

#[derive(Debug)]
enum H264ParserState {
    Init,
    OneZero,
    TwoZeros,
    StartCode,
}

// Creates an iterator over NAL units
pub struct H264NalParser {
    data: Vec<u8>,
    read_offset: usize,
    state: H264ParserState, 
}

impl H264NalParser {
    pub fn new() -> Self {
        H264NalParser {
            data: Vec::new(),
            read_offset: 0,
            state: H264ParserState::Init,
        }
    }

    // adds new data to the parser
    pub fn push(&mut self, data: &[u8]) {
        self.data.extend_from_slice(data);
    }

    fn to(&mut self, state: H264ParserState) {
        self.state = state;
    }

    // TODO: handle emulation prevention bytes
    fn read_slice_header(&mut self, t: &NALUType) -> Option<SliceType> {
        if t.is_vcl() && self.data.len() > self.read_offset+1 {
            self.read_offset += 1;
            //let slice_hdr = self.data[self.read_offset];
            let mut bitreader = BitReader::new(&self.data[self.read_offset..]);
            match bitreader.read_ue() {
                Ok(nr) => {
                    let first_mb_in_slice = nr == 0;

                    match *t {
                        NALUType::CodedSliceOfIDRPicture if first_mb_in_slice => {
                            return Some(SliceType::ISliceType);
                        },
                        NALUType::CodedSliceOfNonIDRPicture(_) => {
                            return bitreader.read_ue()
                                .map(|res| SliceType::from_u8(res as u8 % 5))
                                .ok();
                        },
                        _ => {
                            // noop
                        }
                    }
                },
                Err(_e) => {
                    //eprintln!("error reading first_mb_in_slice: {e}");
                },
            };

        }
        None
    }
}

impl Iterator for &mut H264NalParser {
    type Item = (NALUType, Option<SliceType>); // start and end offset within self.data

    fn next(&mut self) -> Option<(NALUType, Option<SliceType>)> {

        while self.read_offset < self.data.len() {
            let b = self.data[self.read_offset];

            match (&self.state, b) {
                (H264ParserState::Init, 0) => self.to(H264ParserState::OneZero),
                (H264ParserState::OneZero, 0) => self.to(H264ParserState::TwoZeros),
                (H264ParserState::TwoZeros, 0) => self.to(H264ParserState::TwoZeros),
                (H264ParserState::TwoZeros, 1) => self.to(H264ParserState::StartCode),
                (H264ParserState::StartCode, b) => { 
                    let t = NALUType::from_u8(b);
                    self.to(H264ParserState::Init);
                    let slice_hdr = self.read_slice_header(&t);
                    return Some((t, slice_hdr));
                },
                (_, _) => {
                    self.to(H264ParserState::Init);
                },
            }

            self.read_offset += 1;
        }

        self.data.clear();
        self.read_offset = 0;
        None
    }
}

#[derive(Debug)]
pub struct BitReader<'a> {
    bytes: &'a [u8],
    byte_offset: usize,
    bit_offset: usize,
}

impl<'a> BitReader<'a> {
    pub fn new(bytes: &'a [u8]) -> Self {
        BitReader {
            bytes,
            byte_offset: 0,
            bit_offset: 0,
        }
    }

    pub fn read_bit(&mut self) -> Result<u8, String> {
        if self.bit_offset == 8 {
            self.byte_offset += 1;
            self.bit_offset = 0;
        }

        if self.byte_offset == self.bytes.len() {
            return Err("No more bits".to_string());
        }
        let byte = self.bytes[self.byte_offset];

        let bit = (byte >> (7 - self.bit_offset)) & 0b1;
        self.bit_offset += 1;
        Ok(bit)
    }

    // 1B Exponential-Golomb reader
    pub fn read_ue(&mut self) -> Result<u64, String> {
        let mut leading_zeros = 0;
        while 0 == self.read_bit()? {
            leading_zeros += 1;
        }

        let mut n = 0;
        for _ in 0..leading_zeros {
            let bit = self.read_bit()?;
            n = (n << 1) | u64::from(bit);
        }

        n += 2u64.pow(leading_zeros) - 1;
        Ok(n)
    }
}

#[test]
fn test_read_ue() {
    assert_eq!(BitReader::new(&[0b1000_0000]).read_ue().unwrap(), 0);
    assert_eq!(BitReader::new(&[0b0010_1000]).read_ue().unwrap(), 4);
    assert_eq!(BitReader::new(&[0b0001_1100]).read_ue().unwrap(), 13);
}

