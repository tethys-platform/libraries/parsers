use log::debug;

#[derive(Debug, PartialEq)]
pub enum OBUType {
    Reserved,
    SequenceHeader,
    TemporalDelimiter,
    FrameHeader,
    TileGroup,
    Metadata,
    Frame,
    RedundantFrameHeader,
    TileList,
    Padding,
}

impl OBUType {
    fn from_u8(b: u8) -> OBUType {
        match b {
            0  => OBUType::Reserved,
            1  => OBUType::SequenceHeader,
            2  => OBUType::TemporalDelimiter,
            3  => OBUType::FrameHeader,
            4  => OBUType::TileGroup,
            5  => OBUType::Metadata,
            6  => OBUType::Frame,
            7  => OBUType::RedundantFrameHeader,
            8  => OBUType::TileList,
            15 => OBUType::Padding,
            _  => OBUType::Reserved,
        }
    }
}

#[derive(Debug)]
pub enum FrameType {
    KeyFrame,
    InterFrame,
    IntraOnlyFrame,
    SwitchFrame,
    Undefined,
}

impl FrameType {
    fn from_u8(b: u8) -> FrameType {
        match b {
            0 => FrameType::KeyFrame,
            1 => FrameType::InterFrame,
            2 => FrameType::IntraOnlyFrame,
            3 => FrameType::SwitchFrame,
            _ => FrameType::Undefined,
        }
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
struct Segment {
    start: usize,
    end: usize,
}

#[allow(dead_code)]
impl Segment {
    fn is_empty(&self) -> bool {
        self.start == self.end
    }

    fn skip(&mut self, size: usize) -> Result<Segment, AV1ParserError> {
        if self.start+size > self.end {
            return Err(AV1ParserError::BufferTooShort);
        }

        let start = self.start;
        self.start += size;
        let end = self.start;
        Ok(Segment { start, end })
    }
}

#[derive(Debug)]
pub struct AV1OBUParser {
    data: Vec<u8>,
    read_offset: usize,
}

impl AV1OBUParser {
    pub fn new() -> Self {
        AV1OBUParser {
            data: Vec::new(),
            read_offset: 0,
        }
    }

    // adds new data to the parser
    pub fn push(&mut self, data: &[u8]) {
        self.data.extend_from_slice(data);
    }

    fn skip(&mut self, num_bytes: usize) -> Result<Segment, AV1ParserError> {
        let start = self.read_offset;
        self.read_offset += num_bytes;

        if self.read_offset <= self.data.len() {
            Ok(Segment { start, end: start + num_bytes })
        } else {
            Err(AV1ParserError::BufferTooShort)
        }
    }


    //TODO: emulation prevention bytes (if we add them)
    fn next_inner_obu(&mut self)
        -> Result<Option<(OBUType, Option<FrameType>)>, AV1ParserError> 
    {
        if self.read_offset >= self.data.len() {
            self.data.clear();
            self.read_offset = 0;
            return Ok(None);
        }

        let (obu_type, extension_flag, has_size) = parse_obu_header(self.data[self.read_offset])?;
        self.skip(1)?;

        if extension_flag {
            self.skip(1)?;
        }
        let mut size = 0usize;

        if has_size {
            let (sz, consumed) = read_leb128_size(&self.data[self.read_offset..])?;
            self.skip(consumed)?;
            size = sz;
        }

        let mut frame_type = None;

        if obu_type == OBUType::Frame {
            let byte = self.data[self.read_offset];
            let show_existing_frame = (byte & 0b1000_000) != 0;
            if show_existing_frame {
                frame_type = Some(FrameType::Undefined);
            } else {
                frame_type = Some(FrameType::from_u8((byte & 0b0110_0000) >> 5));
            }
        }

        self.skip(size)?;

        Ok(Some((obu_type, frame_type)))
    }
}

#[derive(Debug)]
enum AV1ParserError {
    ForbiddenBitSet,
    BufferTooShort,
}

fn parse_obu_header(b: u8) -> Result<(OBUType, bool, bool), AV1ParserError> {
    let forbidden_bit = (b & 0b1000_0000) != 0;

    if forbidden_bit {
        return Err(AV1ParserError::ForbiddenBitSet);
    }
    
    let obu_type = OBUType::from_u8((b & 0b0111_1000) >> 3);
    let extension_flag = (b & 0b0000_0100) != 0;
    let has_size_flag  = (b & 0b0000_0010) != 0;
    //let reserved_bit = b & 0b0000_0001;
    Ok((obu_type, extension_flag, has_size_flag))
}

fn read_leb128_size(buf: &[u8]) -> Result<(usize, usize), AV1ParserError> {
    let mut size: usize = 0;
    let mut consumed: usize = 0;

    for i in 0..8 {
        if i >= buf.len() {
            return Err(AV1ParserError::BufferTooShort);
        }
        let b = buf[i];
        size += ((b & 0x7f) as usize) << (i*7);
        consumed += 1;

        if (b & 0x80) == 0 {
            break;
        }
    }

    Ok((size, consumed))
}

#[test]
fn test_leb128() {
    let res = read_leb128_size(&[8]).unwrap();
    assert_eq!(8, res.0);
    assert_eq!(1, res.1);

    let res = read_leb128_size(&[0b1000_0000, 0b0000_0001]).unwrap();
    assert_eq!(2, res.1);
    assert_eq!(128, res.0);
}

impl Iterator for &mut AV1OBUParser {
    type Item = (OBUType, Option<FrameType>);

    fn next(&mut self) -> Option<Self::Item> {
        self.next_inner_obu()
            .map_err(|e| debug!("Error parsing AV1 packet at byte {}: {:?}", self.read_offset, e))
            .ok()
            .flatten()
    }
}


#[test]
fn test_av1_parser() {
        use hex_literal::*;
        const DATA: [u8; 6577] = hex!("
            120032ac333004080a72ee49520882082009000400c180002359991c3c2b5756
            c26b08e9ff0fa951c09ec389c35e78f533ad1cb6acbb12687b4f253aa51f7178
            c53dbbb1b140bce40e781f99ddcb95a5307e92e546bafd2ac778f0919d004269
            0578cc72121ec3e2f79d39bf88d1bfff682688f16e1a88c9fba3adcc7ee5b146
            bd7e2170f22240c1735d81ba8c091de8265d33beca45ee1b47fed610edfed1e2
            790ea1ad2b5a18c358b99c4c9ab0dd48365b28631e1d09b73175280ae2da13bc
            b923e7d3d4019e6e8524f69582f2b92f1363398c4199b9078495e0939d0e3a2d
            737d1c76c5c816973c8594ac98d05fb9feb80b14cc1d134715624c22e7f7c911
            42bd37494b4eaffa7a5610e33eb854d33f36e72eaedd9b568c7a546d232161dc
            5e3d806a91f1161f77edacdb9f289a9e63999c3972e8aed3948447fc18bdb6d4
            ce169f0ef14d4e5119354bc45410772331bbe099b04a4cacc0afff4582bea319
            bafa747161b76fc9c6466dcb371c88958736deaf37ff4e3dc47ab0848af3a339
            5531080ef3ada2466a1c8f7316c1a16933291ffdafb97f170904caae03013357
            784216255fe9d513dbde6834051b9a57bf4aa9dea28df6a5ad05f90b34eb5768
            469fc39586dcc9ebcbe5eca64048dfc18c7068948fd6de6676d4ee47c13ba64c
            b6ec62f9ea607b314c02bb297e3ded908b98e2c75a6216daf8c157bec7c85768
            45ed738deed55cdf4a64dd400053b29c2c329a7263e95c61ebbc24745b630cf8
            240e9e68b0571bde63c91683e32a6488a71147c88019e1990555136fc6014020
            ef6ea658d992dad1baa3c1f0d6bb7f395243350fa20fc98efaf2823fb902ce96
            4e42f89d0cc267438b3fcce9cf6f790278f35307b598a7a19d27e3b68c3d071a
            5a24679ad3ac128af2b7b840c513eb4a7bbf8552b4a2bafd326a2e2e942e06eb
            948571cd5f315936d2bc62553efaebed67d4a32d4473aafc85a2a0b416ad3537
            3b26e493624d3418937e94fe1a7062c2dc0b8a3b9da48e796d3573407e9b3b7c
            988cd0f171eb1a4b6a3f8e4e90d27dfa832b1bfaa3b9544848ef01f993765f42
            8d917868e6b13266102ab63bba633e4088c54a48bb74e79ea61f4e9faa419ed8
            9a74e208ec84b8d4f29f7a42c21f1aa11d9ade2a3b38ef0653210c0ffda12835
            4b10bdb06da287332a47ee21daf82bf02e421f67a8d19e11e8ce08b7838843e4
            f328f873b7e06a8a12ecdf015918110c4e0e9b5cea88ea6df0473c2c09ca853b
            0450121bd828733ba9a051d4c35f4ca3d84fb36722da3771ac707d5e4ae186ea
            0cd875e09a7b546d3f113a497e99d26bd80ce72209e4f844c7a91702faeb0296
            6e997ad1c60fc621395b76cdf4131f7d4a465c679cf42d73e49869a7ad49e382
            336abaf3ae617909835a4e35e97cf941993aade02f55c0b1b5ec44645db36de8
            ab9a77e2e3d0910fe935f008794000619e9fb16c54d7166a37a09db81a6e97b0
            213b018ed4f1ff58e95ef79e43824dd0679d9a2eb0ef1130a03345d91c5e9961
            4d16661ec45d5f631faedb4c5dccbea8a25d0c63227c9c95f91f3ac7e73690ac
            52138bb5ef5408267b45944b787a534e9a33eab1804e502f750217220444f5f8
            d9bc583046b67769013c894a1487a412270b1b154a80c2a4e46f68055f1acb79
            1d3575b414d248b05f87ff92d6c5d34c7c07c420358aba27ab03ce40e0c89f36
            7f2f92a9b279a6e868d872749996e681558e7ad65c608cc6e969aee7a6a926d4
            6a0938526ad83452e5934f04bbcfdf30b53835bab68b0840a3cf6702c71d498c
            26eddf9bd9bdfa92225107085871877638e243497c4cae32af7df273579431b7
            98452ba8f376d081a8cd3a3cf617b396899e757996bcfadcad3fa43ad59c2834
            a20df37a6d2a00db800701fe5cbf0d148650318008a5a0a66c49ecca6b300f0e
            8295836551904c16e9c191fc5c2cd3214913ab8508aca10b72a5b567aa107aa1
            f58035fb937549cfb939e05c6b551fe2b7b3200f2b296bbec7d708f1b7971a72
            0a1e32d0aed8621409a7b9933e9deab922e91b781c784d41bd5c8551a3f773e9
            48788bde7fc9785d066dd2c77178984945797016cb86a3b351c103d9a76d62f8
            20515ea38d32e6c2733181999230fa91c67b394d6063ed63856ae3240b368013
            28d667c22b3b2aa55a4359f065597010a70f997f81e8d7f13bf0070892529e27
            d726a33d426265fbafe94447dc91e964ea05b673b6bdf978f6aad55772f511f7
            2fc5a5e022de01e17b6b34fef5dde364fe571e3c52952ab2bc79f127135bfe8e
            4a312b8f9511495f97ccaa0e8979b2a9cef6f69d05ee779a1fa3392b669ce500
            aac3fc541fb0faadd84e6949b45eceb2bbb25ef2e01773b940f4aaaf3c433030
            05eeb94e48b49a570dcbb60f43c45d512726a631e4f02df3eb03acef2a4849e1
            1e80d21268dd8270817c528911f5249e4f090362aa7e304fb116693ed72f2c25
            4a6d05cafd34c358041ada427ca59f2faaee4566298a82423399d4ad4d4b07ac
            003c5eed4aa79e9275f714ea7eb7944fd502d81271ea87aedfbc5fd843c49ab0
            6b51d8fded1dc241cd1742302b7c942d531caf1470cfc273082d91a2be3f938e
            87cc671abf9a596adb57552a7036b84305fff574fedfac821ef2547d48ea011f
            6e748f5a78af92fd5b8c3918858f09d49178f3b3f2dc32eaa96749fe51ca8f32
            fde18e0ea116a0639038a3b1935fa0b811cddd9faeb2f85d25e733b5de309f3c
            ed787296a060df00203e2d41460316b1c1699c45e059083261b4c2facd8ff318
            e67553965b140788f1b61d2ee61594ded794346e0904e6ee86db8172442df4d9
            4cf8b0d8018b519520d3fafd72e882b224f2473e573339802e61507bf809a05b
            78823da73d633c487a71350303988f39b9f2f9689ccd7d84495ba29229272ac9
            5233ebe8b094a93830235296524ec110f81c5141a2c28a31fa00387d47ef19e5
            e2f22a4a0c51e9fbfacb54b46393fd79cb88ed747aa150caa2ec38e062f2fa38
            b1a982d058e232d82131c211e51b4b9a4836cac05c356dd9e2c94bdfc498d058
            3e544e935e768fdcdfd7df75c8fb98e5466ee83edd7a75ef218b6b1f0bad6f51
            567644622bb6ff5ccb2834741a91307467ef56468424d6317dfef39e1cb555c9
            11999740acd76a6524fbead8e69eaf34bf124f64bfdcace7100ff3fbdb2bc1b3
            4bd23a3f5ca687a27905f5994f66c9f19c19c23f5be096fc80d4e1067c158b8d
            8c42226f817961d3ca7f6067a8c1738b88c1d3839dfa7168c4d3c36749e940cf
            dc07ead464ab6ea69acfcb74879e27fa7dcfc7ef306a7b73d8c20bf0c002e7db
            290f51a46bb16bab19347d457546bf9ed65bd84a340eb2af9c7a853bd79b2724
            8042e7f506155b6061bf51d69eda329ac1f86223ea0c0d4e2ff8ca74c5dfbe87
            eb0f2d367300ff3d79caabcea122e0f0034487371dd3c6618c0e907b4404fe76
            05412a17ed9f16f03e5f4a066831ccc190818d445422855f41d559166b1643d2
            d18d13814d27e9e238a94648ef2ce256b91c146745347499cdb37e8611833cea
            b85af05d66da8e9cf80d6554b2e17d41d73c0763f987f8b48fc5172ba9fc1d36
            47accb4b2bf17a3414a89ce717978a77564f77712b517a24e45b00c1d09be722
            5145fe9ae49516d1a64f7de1ec54e71a92ef62c0e3d2bd6ed558b120844c3efb
            100fd8c30555b874554da7bf7c1c94f4fe04f225e4ee0ed5d70e957f476dac92
            a07f682073d223febfc1803c478d5a2e796a871ca34403d45c76c881fa6c9071
            ef47da4feef5759c8faf7d9ffbae3478eead4c6423a96027f091eb38ba104829
            c4670c1145f3eaebcf201a48fa4711c17420fd918bd631708ff978d6a0361ed5
            81bf38e315c3d812994a0200608d618871cb0b52f5a3c9573588a3c97c6ff399
            3aaceaa09a2cfca008352cb9564d448bedafe2cd1e4d8826730735d744d24ed6
            ead28212e0dd3e17d218915a7806faebcd93930bb4588d12702a329d6d6827cd
            8f10188eaf0bec42f5f81e8cc76240449e8e1577b2ca72704f90c840db151602
            0f590b917522a60e73582595c1cdb04e19df0095edb54dbecd7125cb1c73ea80
            ff59f2abc7881655dcbf1434ab4a9bc00fb8311008904192c03b4178f85c4e60
            8e5d20da10751ad3f6ee5d05996838f3030d49cbdc68f47824d49371b563a6a8
            cda202b5a76177465b3e6eb32cc83386cc6177c14d454e55fd923e2134b2e5a2
            c9ff02862b7db595b0a49a2a8b3f69b79d5296a9c248ad5edc64acdc39a32398
            fc46978fe5c318a7a724fae9f2060a8bb226271d5639e230c15f40a7a65d4f4e
            1bded014644714ba87bd3889831f41017850a05f9071150387dbc61cea54880b
            c112c319efe73bbcc877d9e19b19c487e41032895966c35a80064b956cc09249
            21cd076e8e8e6c1457c239cc7fb6f072f36946a76e17f2a1b96261a91e3be731
            409b25f3f0e194d236449a03a8818039401672413ba5bc4f8cd92f42d9623376
            e97a0b91eee4be0fbbb18cb4f1052d220099e057646c66bcdc278d1a6e3f5e82
            2b0a15f45764caf7445bf12a91b870856faef8eacca8c707057a57235f65b6c3
            f1b6d9c5dbba22975d4d2822d20a6c907c1dcbfc74db568694dab26571a50770
            c3dcbe6b9f9461cdb60cadeb5b68396db80155a9218fb3b07d3afb936bd2522f
            4beb36a51e84495bdad5ef7a6f2021152e0f6cbb32c35225ff7278753b14f120
            3f2bc07267ce2757d6b893ccbf4589785eb577234e655dcf6bbf4119fa8a8936
            4c4d693d24fe3410c8e5c1dadcff63463a5c98c957564fc078885a3fa81ab8c9
            b7d57655acf4e59c10c8ec265984abca6f2d25a83076676a6a223cbe9bf27a22
            d0b8d3588d5304e0e454ecef37b151111ff63c0d77b994f1d16f14c08a040e16
            8cb8c244a7ae54dadb0dd5106cde42e3cc871e704f714e250f40d9fe314d4d85
            f623dce64ea724864618289c215d0d2698b7a36e8b7acbd99f98bf6a858faa01
            237ca429888a8d653fdbd384ad031a2e17628fe33fbfe1c4b10441b831c6d097
            3c66e91358bc1105d93379e9c7248065f95f9902327f88120b2a32951a6e584f
            706cc3d548e368c9229028c9c8f3ba890bb44ab4c08a3b8678dfc55d421f573a
            55ab438f27114e9c7dec75b37587cfc3b26bc1b96c5ddaa2e34fe32324bf3320
            11ff10e8d46116c3e372a27ce5cf09b6a45fd6dd91d37b3a143d507d18f97883
            17c1f07840772dc3b1ac6c320c461e67b997d37f5b7456a2bcd1134bf9190e61
            1ec6f4f2ff892c8f160c91aee457b0d042d6f4c912f74e86415cd869ee47819c
            87885a9adc0c8fbb55aa4c76e22e1e4bd1e3da4d6acd8e30e9a67860aff25b99
            8d866f47f3ad32326ab282d67946b2dae4364cce84eca91c2f8139311153a61d
            fa7ffb0a54047a3660e72275fc7c2c32e115085ef92f8dfc18bc8710e217b916
            a75d55b19f49888e73e49b746a9c8ed769308696e37a833dfbe80cde78eaacdf
            9534383fb735da391c757f9c618b86fce6f93565558a0851ea1df464fee63a50
            66f48dcd361efd3c9aceafa6c090b8dd29dc2b9f48d841489842274f00e2ad42
            0db3444175cc72c957171c6ba39fbb4cd0b086a40ad30fa5298319ba7e485196
            c61d25e540bff55742186238841b324c495a80a25a2ef2e156816edf89ec9bc7
            ccff48733258a71b0fc16b0b3b11d04856f180b6e771f670c3c8d89f23bc2099
            efcbc30c9b6d19038ec8d26f2516a42870fa7d5fba7d6707a361e879f06e0825
            e6943a534c41ea3ae1ddb68dc896b4849f034a696d58a7095d7b4ca5489928a5
            689bf5b90d5ee0fe77009fbf86fc179a3a76a88502f792e8ff9fb3c5e81f08ca
            8bf5fe50b42fd46d8c9310c94e7237a529ce49bc11df9aabc495cb8c65427208
            2a5ea62e662fa4e986e6a5eaa3afd7b6ed71223bcff3796b7f9db869cf083fb3
            d740fd01a129d7d5dfc61f3474b3a2570c4e6abfcf1ba066accbf224ebe54a8b
            5529f514de33949f66bca722f7bff8d38e547dae9a51512f30ace191fdc38dd0
            f40ba29eaf7c963fb18c2a1291c992d323c88e58250d9aa29571b31d95609c7d
            a0c18a1d649fadabe47ee8256458b3489d230d7b6a8e8b91474534a9c1f79355
            61c293029975ad78cff6828e63b49614569f3a7d85c9f62660d082842c61eafd
            6479a0d8a2ff8e232c58b6bf6e9fa61bdb6299e7eb980f68700a3025cc9719f6
            f7802fc6e6c9df68fb83409cc94d03f3afe4bae7f48fe22198f4da4666e80506
            b8c5212b8642762d78f168eabc9713e6afec2eb27bb6d2ce9b9f3cb5aac567a2
            5d283d8bd3c0423c4daa1a4d363c353e5ce3a6c9bd203718d8db8544d384650a
            df7055e0b12e3aef963172daa55179ce1f587d2ed6f23c6cd5e1463d7970f1bb
            bc32bc6841c42aef192d219cab7b8f71c0f69028ff7b8e066a86b819ecb46a8a
            a8d8787cce4098e033addd82de99970549694ecb926d781a7c811b022735f950
            3d244670b67782912d46392f696c4521ae0603abfc86898c4e165f88f08a7c64
            7da8cb99aebdc79ed834542a4546ee007b7a19e77f3ff1eb2cd416d1ce464355
            b485d26c5d883afa9ae918b6bcacc4fa8b0b269c8782d11abc98f2db1afdd3a9
            0c5a4b04f5bb81e77a47ceb41af7701fc3d7a699225e7cfdfefd9489b0331cd8
            0f054ac4df8655c228cdd48c6e899bce2523e628b9a7ffdf55adb27b2220a7aa
            fb890c94514c22e05a7ba3e22c14897c81054da410b4d364d655481eb70c81ee
            2d76aabeb86919c1a092408349525208c4b2e4cfde5d59e482b20e94084a8ad0
            f9e3e83eebbdf076de336fd3d3bbf4cb6aa365392ba54f817817c9253db2d307
            6190f8ed105d524e70e37737b2a1e8b3189c062a5ed8fae7d11cc174e3ba40de
            a98c56ff5ffd93033d4e6a638f888cf30796ea0c26e01ad2ef91e416faafcfd8
            741d092efd9320066420e9a9cf673f085dbac5877d4a4286816ff8e67d826500
            12d5bf9f8ae0879031e03664ad12efb0d4fa002e98189f1c25b7f2fafdff2647
            4b0a5a80ed669ed31c545bc06e0eaca82889c5a02728ea193e658a5ca3e9d907
            0776f9ca03eba0e61874d16f4f6f76807e8d4961c24cc2a436ddd700de0b9395
            14b0368bf3aed020e8c081c8a3a8807d916261779ca7ddc415ec8c8a4c1a730a
            be6851c3f171b2ef8f86a0a161b779b8fbb0696c88fa62e59576122ed9abe489
            1436ce1ff36625b16ee880a744e1200ea63e83ca84f8e012d60d2cd10dbc7e58
            f9041aa371142e08864fdb078034e2853dbde442658d78b155abfb31c269026b
            7192726555d1dda278ada31542baaa66a52ec6a954f6d82aca0b657ab29eea36
            c764293ddc56518c148b15f887905091b6c57114ec1e038abfb6a09e29779171
            4648af8b0dac410a289f6e86fc73b95cdeb7b4728e933a51eb1a61d83ea69a89
            1929334cea84efe8f9039e5ebccc8fdc7d20bb76f878489feb0893fc91e5beb3
            2745c92b368aabda85db522d65a6ff5e7eaf1e36cb8fe5aabe8b393994c4e74c
            ea142eb06669564e4196184f423d93d8b5d28622ff8c83dbec0c0ca546ef2a6a
            75e66d0db896fda8e63b3d6c3d8c9b3bd0b280218a513695cd8211db8a52a471
            20beeecd228afcc76db16e77fc4ebbd5cf1da269204d877ae4ec09b5d7ebead7
            eee3dfc64a1a9f488a55add2f1d9b54da1fddc1a1eafd366af163ba92d780246
            af45ed113967eb706cfd9ad7f349c55ba9cede81b3740c5e7c9b821425d84293
            e0087cd5a75e49cc21535d669c57fb9ed3eb2fe7a5619df45acd42b09c87d02f
            004595fa14690cc6bf7ce413fb03ad9e4b2191fc9986840b2f5e827ea79e16f2
            d5c103bf3611abe114d602b69f513940f3468554848068be3de17f0cfb14a4ed
            4da921b04e5f434539c79eca818542dc62ed72de26a26c0ae39db00079a7f575
            932df09ff9fc595b8976fa16fc9b7337376b9c3adcc8a10cfb5d8257d31e7fcb
            f8325447f3a8712ceb3cf6e79a136eafa3125bb9a8e03a7387d6d5b3f68f3194
            3373105f3f624c8a376017312dabfbb193e17dd6d8257bd64238e55b46bcda6c
            5684c1c0ab857956000cb9732ddc4ec1c773559e360918e453845743a0c2552b
            b352d091473f9712ed8bde90812e90a1e93ace85b9aba121c2ebe461c8ac3d73
            ff232459b088fb968a1609d3706b9aad04f42d3e6755fbc7b574dfd35a0c9ae9
            57eb14d3b1a673356ed932aa03db75070b65efde3fd10511c4b78654e713b2ea
            523304a4725209c1421781bfd7d93e652411b1605dfecba362ad18a56cb4ad87
            d73787480a6b7eebaa361fea9866ef733446c32b220e6eb49e895be110de4e58
            97fdfe94cb1918e22b57690dab774d4ff043cc0d6efc720dbc9e812f0161120a
            bef12a5d44997c4fbe9d666da95729f3ad4dd0977f9d1e7e6ab58a992e576f0a
            24603106239ac659a4607ee99e2e62ac40ce439f8c1f62392a3d00ca67870047
            5c1a0d0b54339cb58d7b3989cd866427ed47f3ae715a2d61ab1a1e51b9887fa2
            4c577137dbefea506bb75a0368514c701934d593418273f1b123674bdf26115d
            15fc4984acdc406d8b895bbeba684c47929a4e6643fa64655ee7448b6adee685
            adae7d0d869e946d5cfbd5c9d64a2851f30bb772d1c108f79d23712454ef3a75
            e8fdc08d2b1cd7c8f76b19586938dde1e85bf641bf92285de3ba022231e99aa8
            e5b440c57f702432b5403860e263676d5bdc2364262d0c0d339bf789cee8fcef
            d391fd816d49e8dd3ef60124d9dd49e04706b563c49dbef86803f804a92fc62a
            e9ce57f722d83db4c4b76d90317eb623c85ad7f3ced76279f72aaefcf36733b3
            c51235c99eb1de30522dc970703a09efe46be40051e7744b8b193d1bbdf6aff5
            68ebe9b8e41a7a98a97e563c9510eb468af5f607f1f5ddb12f889153a03bfca3
            3743d2301158ca2971075dc77b96378323413c7033553374177030f7b819e7fa
            397f94cc0cc867c376b783990bf3743324734bce298c9fbdd2d07baebfb34d46
            81ee264fc0eea03f787813b4aadd4740a70c0c8a4b16341e111a8341fa9a4c48
            46d2c3356973630a0e3da8eebed8be6f2e131a841b39112e7fd493b6a66b21d2
            9d214bcf84ba79966f4e556aa82f7ca85e86406c164f5c9e73f15de1d27d68f0
            66a500f91662c5058cf37cf1b2664bdfad90f5bbf7b1e4051aeb912d5abdcafd
            3615c5ada14ab0f45374160509a03caca809609bd6249e20781b3ad8ef9540f1
            b3b30098dfbbe42955cac78efd66b16938");

        let mut parser = AV1OBUParser::new();

        parser.push(&DATA[..]);
        let mut obu_count = 0usize;

        loop {
            let next = parser.next_inner_obu();
            eprintln!("{next:?}");

            match next {
                Err(e) => {
                    eprintln!("error: {e:?}");
                    break;
                },
                Ok(None) => {
                    break;
                },
                _ => {
                    obu_count += 1;
                }
            }
        }

        assert_eq!(2, obu_count);
}
