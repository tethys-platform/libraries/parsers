use log::trace;

pub struct Parser {
    pub last_pat: Option<[u8; 188]>,
    pub last_pmt: Option<[u8; 188]>,
    pub pmt_pid: Option<u16>,
    pub stream_types: std::collections::HashMap<u16, Codec>,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Codec {
    H264,
    H265,
    AV1,
    KlvMauc,
    Klv,
    Other,
}

impl Codec {
    fn from_u8(stype: u8) -> Codec {
        match stype {
            0x1b => Codec::H264,
            0x24 => Codec::H265,
            0xa1 => Codec::AV1, // MotionDSP extension of the standard
            0x06 => Codec::Klv,
            0x15 => Codec::KlvMauc,
            _ => Codec::Other,
        }
    }

    pub fn to_string(&self) -> String {
        match self {
            Codec::H264 => "h264".to_string(),
            Codec::H265 => "h265".to_string(),
            Codec::AV1  => "AV1".to_string(),
            Codec::Klv  => "klv".to_string(),
            Codec::KlvMauc => "klvMauc".to_string(),
            Codec::Other => "Other".to_string(),
        }
    }
}

#[derive(Debug)]
pub struct ProgramDesc {
    pub stream_type: Codec,
    pub pid: u16,
}

#[derive(Debug)]
pub enum Payload<'a> {
    Pes(PesHeader<'a>),
    Data(&'a [u8]),
    // PMT pid
    Pat(u16),
    // PCR pid, descriptors
    Pmt(u16, Vec<ProgramDesc>),
}

#[derive(Debug)]
pub struct PesHeader<'a> {
    pub sid_type: SidType,
    pub pts: Option<u64>,
    pub dts: Option<u64>,
    pub data_offset: usize,
    pub data: &'a [u8],
    pub total_size: usize,
}

#[derive(Debug)]
pub struct AdaptationField {
    pub random_access_indicator: bool,
}

#[derive(Debug)]
pub struct TsPacket<'a> {
    pub pid: u16,
    pub cc: u8,
    pub adaptation: Option<AdaptationField>,
    pub payload: Option<Payload<'a>>,
}

impl<'a> TsPacket<'a> {
    fn payload_data(&self) -> Option<&'a [u8]> {
        match self.payload {
            Some(Payload::Pes(PesHeader { data, ..})) => Some(data),
            Some(Payload::Data(data)) => Some(data),
            _ => None,
        }
    }
}

#[derive(Debug,PartialEq)]
pub enum SidType {
    Video,
    Audio,
    Klv,
    Other,
}

#[derive(Debug)]
pub enum TsParseError {
    NotEnoughData,
    BadSyncByte,
    ErrorIndicatorSet,
    UnexpectedValue,
}

fn parse_psi(data: &[u8]) -> Result<(u8, &[u8]), TsParseError> {
    let pointer_field = data[0] as usize;

    if pointer_field >= data.len() {
        trace!("PSI: bad pointer_field length");
        return Err(TsParseError::UnexpectedValue);
    }

    let psi = &data[1+pointer_field..];

    if psi.len() < 4 {
        trace!("PSI: bad psi slice length");
        return Err(TsParseError::NotEnoughData);
    }

    let table_id = psi[0];
    let flags = psi[1];

    // See sections "Section stx indicator" - "Section len unused" in the table above
    if (flags & 0b1111_1100) != 0b1011_0000 {
        trace!("PSI: bad flags: {:b}", flags & 0b1111_1100);
        return Err(TsParseError::BadSyncByte);
    }

    let section_length = read_length(&psi[1..3]) as usize;

    if section_length == 0 || section_length+3 > psi.len() {
        trace!("PSI: bad section_length");
        return Err(TsParseError::UnexpectedValue);
    }

    if !check_crc32(&psi[0..section_length+3-4], &psi[section_length+3-4..]) {
        trace!("PSI: bad crc32");
        return Err(TsParseError::UnexpectedValue);
    }

    let table_syntax = &psi[3..section_length+3];
    Ok((table_id, table_syntax))
}

/* == Elementary Stream Specific Data ==
 * Name                 |  Bits | Value
 * stream type          |     8 | 
 * reserved bits        |     3 |
 * elementary pid       |    13 |
 * reserved bits        |     4 |
 * ES info length unused|     2 |
 * ES info length       |    10 |
 * ES Desc              |   N*8 |
 */
fn decode_program_info(data: &[u8], program_info_len: usize) -> Result<Vec<ProgramDesc>, TsParseError> {
    if program_info_len > data.len() {
        return Err(TsParseError::NotEnoughData);
    }

    let mut curr = &data[program_info_len..];
    let mut res = vec![];

    loop {
        if curr.len() < 5 {
            break;
        }
        
        let stream_type = Codec::from_u8(curr[0]);
        let pid = u16::from_be_bytes([curr[1] & 0b0001_1111, curr[2]]);
        let es_info_len = u16::from_be_bytes([curr[3] & 0b0000_1111, curr[4]]) as usize;

        if es_info_len + 5 > curr.len() {
            break;
        }
        
        curr = &curr[es_info_len + 5..];
        res.push(ProgramDesc { stream_type, pid });
    }

    Ok(res)
}

impl<'a> Parser {
    pub fn new() -> Parser {
        Parser {
            last_pat: None,
            last_pmt: None,
            pmt_pid: None, 
            stream_types: Default::default(),
        }
    }

    /* PAT - program association table
     * We parse this only to get the PMT PID
     * 
     * == PAT Syntax ==
     * Name                 |  Bits | Value
     * Pointer field        |     8 | Present at the start of the TS packet payload signaled by the
     *                      |       | payload_unit_start_indicator bit in the TS header. Used to set packet alignment bytes or
     *                      |       | before the start of tabled payload data. 
     * Pointer filler bytes |   N*8 | Filler bytes, 0xFF
     * Table ID             |     8 | Table identifier
     * Section stx indicator|     1 | For PAT, PMT, CAT set to 1
     * Private bit          |     1 | For PAT, PMT, CAT - set to 0. Others 1.
     * Reserved bits        |     2 | Set to 0b11
     * Section len unused   |     2 | Set to 0b00
     * Section length       |    10 | The number of bytes that follow for the syn section with CRC value.
     * Table data           |   N*8 | 
     *
     * == Table Syntax ==
     * Table ID extension   |    16 | Informational identifier
     * Reserved bits        |     2 | 0b11
     * Version nr.          |     5 | Syntax version nr
     * Current/next indic.  |     1 | Indicates if data is current or for future use
     * Section number       |     8 | Index indication which table is in a related seq. of tables.
     * Last section number  |     8 | Indicates which is the last tbl in a seq of tables.
     * Table data           |   N*8 | Data as defined by Table identifier
     * CRC32                |    32 | Checksum of the entire table excl the pointer field and filler bytes
     *
     * == PAT specific data ==
     * Program num          |    16 |
     * Reserved bits        |     3 |
     * Program map PID      |    13 | The packet id that contains the associated PMT <- important
     */ 
    fn parse_pat(&mut self, data: &'a [u8], whole_ts_packet: &'a [u8]) -> Result<Payload<'a>, TsParseError> {
        let (tid, table_syntax) = parse_psi(data)?;

        if tid != 0x00 {
            trace!("PAT: bad table id {}", tid);
            return Err(TsParseError::UnexpectedValue);
        }

        let current_next_indicator = table_syntax[2] & 0b0000_0001 > 0;
        self.pmt_pid = Some(read_pid(&table_syntax[7..9]));

        if !current_next_indicator {
            trace!("pat not current");
        }

        let mut last_pat = [0u8; 188];
        last_pat.copy_from_slice(&whole_ts_packet[0..188]);
        self.last_pat = Some(last_pat);

        Ok(Payload::Pat(self.pmt_pid.unwrap()))
    }


    /* PMT - program map table
     * 
     * == PMT Syntax ==
     * Name                 |  Bits | Value
     * Pointer field        |     8 | Present at the start of the TS packet payload signaled by the
     *                      |       | payload_unit_start_indicator bit in the TS header. Used to set packet alignment bytes or
     *                      |       | before the start of tabled payload data. 
     * Pointer filler bytes |   N*8 | Filler bytes, 0xFF
     * Table ID             |     8 | Table identifier
     * Section stx indicator|     1 | For PAT, PMT, CAT set to 1
     * Private bit          |     1 | For PAT, PMT, CAT - set to 0. Others 1.
     * Reserved bits        |     2 | Set to 0b11
     * Section len unused   |     2 | Set to 0b00
     * Section length       |    10 | The number of bytes that follow for the syn section with CRC value.
     * Table data           |   N*8 | 
     *
     * == PMT Table syntax ==
     * Name                 |  Bits | Value
     * Unused               |     3 | 
     * Program Number       |    13 | 
     * Unused               |     2 |
     * Version nr           |     5 |
     * Current/next ind     |     1 |
     * Section Nr           |     8 |
     * Last section Nr      |     8 |
     * Reserved bits        |     3 | Set to 0x07
     * PCR PID              |    13 | PID carying PCR
     * Reserved bits        |     4 | Set to 0x0F
     * Unused               |     2 | Set to 0
     * Program info length  |    10 | Num. bytes that follow for the prog. desc.
     * Program descriptors  |   N*8 | ..
     * Elementary stream    |   N*8 | Streams used in this program map
     * info data             
     *
     * == Elementary Stream Specific Data ==
     * Name                 |  Bits | Value
     * stream type          |     8 | 
     * reserved bits        |     3 |
     * elementary pid       |    13 |
     * reserved bits        |     4 |
     * ES info length unused|     2 |
     * ES info length       |    10 |
     * ES Desc              |   N*8 |
     */
    fn parse_pmt(&mut self, data: &'a [u8], whole_ts_packet: &'a [u8]) -> Result<Payload<'a>, TsParseError> {
        let (tid, table_syntax) = parse_psi(data)?;

        if tid != 0x02 {
            trace!("PMT: bad table id {}", tid);
            return Err(TsParseError::UnexpectedValue);
        }

        if table_syntax.len() < 10 {
            trace!("PMT: psi table too short");
            return Err(TsParseError::NotEnoughData);
        }

        let mut last_pmt = [0u8; 188];
        last_pmt.copy_from_slice(&whole_ts_packet[0..188]);
        self.last_pmt = Some(last_pmt);
        
        let _prog_nr = u16::from_be_bytes([table_syntax[0] & 0b00011111, table_syntax[1]]);
        // 3B of flags...
        let pcr_pid = u16::from_be_bytes([table_syntax[5] & 0b00011111, table_syntax[6]]);
        let program_info_len = u16::from_be_bytes([table_syntax[7] & 0b00000011, table_syntax[8]]) as usize;
        let pi = decode_program_info(&table_syntax[9..], program_info_len)?;

        for ProgramDesc { stream_type, pid } in pi.iter() {
            self.stream_types.insert(*pid, *stream_type);
        }

        Ok(Payload::Pmt(pcr_pid, pi))
    }

    // parses a TS packet taking PAT and PMT into account
    pub fn parse(&mut self, data: &'a [u8]) -> Result<TsPacket<'a>, TsParseError> {
        self.parse1(data).map(|res| {
            if res.pid == 0 {
                let payload = res.payload_data()
                    .and_then(|payload| self.parse_pat(payload, data).ok());
                TsPacket { payload, ..res }
            } else if self.pmt_pid.is_some() && res.pid == self.pmt_pid.unwrap() {
                let payload = res.payload_data()
                    .and_then(|payload| self.parse_pmt(payload, data).ok());
                TsPacket { payload, ..res }
            } else {
                res
            }
        })
    }

    /*
     * == MPEG-TS Packet Syntax ==
     * Name                 |  Bits | Value
     * Sync Byte            |     8 | 0x47
     * Transport err indic. |     1 | Set when demodulator cant correct errors from FEC data
     * Payload unit start   |     1 | Set when a PES, PSI or DVB-MIP packet begins immediately following this header.
     * Transport priority   |     1 | Set when current packet is higher priority than other packets with same PID
     * PID                  |    13 | Packet identifier
     * Scrambling control   |     2 | 0b00 - not scrambled
     *                      |       | 0b01 - Reserved
     *                      |       | 0b10 - Scrambled with even key
     *                      |       | 0b11 - Scrambled with odd key
     * Adaptation field ctrl|     2 | 0b10 - no adaptation field, payload only
     *                      |       | 0b10 - adaptation field only, no payload
     *                      |       | 0b11 - adaptation field followed by payload
     *                      |       | 0b00 - reserved
     * Continuity counter   |     4 | Sequence number of payload packets, incremented per PID when payload is present.
     *
     * Adaptation Field Syntax (only up to RAI which we are interested in)
     * Adaptation field len |     8 | Number of bytes following this byte
     * Discontinuity idic.  |     1 | Set if current TS packet is in a discont. state with respect
     *                      |       | to either the continutiy counter or the PCR.
     * Random Acces Indic.  |     1 | Set when the stream may be decoded without errors from this point. <-- important
     */
    fn parse1(&mut self, data: &'a [u8]) -> Result<TsPacket<'a>, TsParseError> {
        if data.len() < 188 {
            trace!("TSParser: NotEnoughData");
            return Err(TsParseError::NotEnoughData);
        } 

        if data[0] != 0x47u8 {
            trace!("TSParser: BadSyncByte");
            return Err(TsParseError::BadSyncByte);
        }

        if data[1] & 0b1000_0000 > 0 {
            trace!("TSParser: ErrorIndicatorSet");
            return Err(TsParseError::ErrorIndicatorSet);
        }

        let pid = read_pid(&data[1..3]);

        let payload_unit_start = (data[1] & 0b11000000) == 0b01000000;
        let cc = data[3] & 0b00001111;

        //trace!("PUSI: {:?}", payload_unit_start);

        match data[3] & 0b00110000 {
            0b0001_0000 => { // no adaptation field, payload only
                let payload = get_data(payload_unit_start, &data[4..188]);
                Ok(TsPacket { pid, cc, adaptation: None, payload})
            },
            0b0011_0000 => { // has adaptation field and payload
                let adaptation_length = data[4] as usize;

                if adaptation_length + 5 > 188 {
                    trace!("bad adaptation field length: {}", adaptation_length);
                    Err(TsParseError::UnexpectedValue)
                } else {
                    let random_access_indicator = (data[5] & 0b01000000) != 0;
                    let payload = get_data(payload_unit_start, &data[5+adaptation_length..188]);
                    Ok(TsPacket { 
                        pid, 
                        cc,
                        adaptation: Some(AdaptationField { random_access_indicator }),
                        payload,
                    })
                }
            },
            0b0010_0000 => {
                let random_access_indicator = (data[5] & 0b01000000) != 0;
                Ok(TsPacket { 
                    pid, 
                    cc,
                    adaptation: Some(AdaptationField { random_access_indicator }), 
                    payload: None,
                })
            },
            _ => {
                Err(TsParseError::UnexpectedValue)
            },

        }
    }
}


fn get_data(pusi: bool, data: &[u8]) -> Option<Payload> {
    use crate::pes;

    if !pusi {
        Some(Payload::Data(data))
    } else if data.len() > 3 {
        let sid_type = sid_type(data[3]);
        let mut pts = None;
        let mut dts = None;
        let mut data_offset = 0;
        let mut total_size = 0;
        
        match pes::parse_header(data) {
            Ok(pes::Pes { pts:ppts, dts:pdts, data_offset:dataoff, data_length }) => {
                pts = ppts;
                dts = pdts;
                data_offset = dataoff;
                total_size = data_length;
            },
            Err(e) => {
                trace!("pts parse error: {:?}\n{:?}", e, &data[0..8]);
            }
        }

        Some(Payload::Pes(PesHeader { sid_type, pts, dts, data_offset, data, total_size }))
    } else {
        None
    }
}

pub fn read_pid(data: &[u8]) -> u16 {
    (((data[0] & 0b00011111) as u16) << 8) | data[1] as u16
}

fn read_length(data: &[u8]) -> u16 {
    (((data[0] & 0b00000011) as u16) << 8) | data[1] as u16
}

pub fn sid_type(sid: u8) -> SidType {
    if sid >= 0xE0 && sid <= 0xEF {
        SidType::Video
    } else if sid >= 0xC0 && sid <= 0xDF {
        SidType::Audio
    } else if sid == 0xbd || sid == 0xfc {
        SidType::Klv
    } else {
        SidType::Other
    }
}

const CRC32_TABLE: &[u32] = &[
    0x00000000, 0xB71DC104, 0x6E3B8209, 0xD926430D, 0xDC760413, 0x6B6BC517, 0xB24D861A, 0x0550471E,
    0xB8ED0826, 0x0FF0C922, 0xD6D68A2F, 0x61CB4B2B, 0x649B0C35, 0xD386CD31, 0x0AA08E3C, 0xBDBD4F38,
    0x70DB114C, 0xC7C6D048, 0x1EE09345, 0xA9FD5241, 0xACAD155F, 0x1BB0D45B, 0xC2969756, 0x758B5652,
    0xC836196A, 0x7F2BD86E, 0xA60D9B63, 0x11105A67, 0x14401D79, 0xA35DDC7D, 0x7A7B9F70, 0xCD665E74,
    0xE0B62398, 0x57ABE29C, 0x8E8DA191, 0x39906095, 0x3CC0278B, 0x8BDDE68F, 0x52FBA582, 0xE5E66486,
    0x585B2BBE, 0xEF46EABA, 0x3660A9B7, 0x817D68B3, 0x842D2FAD, 0x3330EEA9, 0xEA16ADA4, 0x5D0B6CA0,
    0x906D32D4, 0x2770F3D0, 0xFE56B0DD, 0x494B71D9, 0x4C1B36C7, 0xFB06F7C3, 0x2220B4CE, 0x953D75CA,
    0x28803AF2, 0x9F9DFBF6, 0x46BBB8FB, 0xF1A679FF, 0xF4F63EE1, 0x43EBFFE5, 0x9ACDBCE8, 0x2DD07DEC,
    0x77708634, 0xC06D4730, 0x194B043D, 0xAE56C539, 0xAB068227, 0x1C1B4323, 0xC53D002E, 0x7220C12A,
    0xCF9D8E12, 0x78804F16, 0xA1A60C1B, 0x16BBCD1F, 0x13EB8A01, 0xA4F64B05, 0x7DD00808, 0xCACDC90C,
    0x07AB9778, 0xB0B6567C, 0x69901571, 0xDE8DD475, 0xDBDD936B, 0x6CC0526F, 0xB5E61162, 0x02FBD066,
    0xBF469F5E, 0x085B5E5A, 0xD17D1D57, 0x6660DC53, 0x63309B4D, 0xD42D5A49, 0x0D0B1944, 0xBA16D840,
    0x97C6A5AC, 0x20DB64A8, 0xF9FD27A5, 0x4EE0E6A1, 0x4BB0A1BF, 0xFCAD60BB, 0x258B23B6, 0x9296E2B2,
    0x2F2BAD8A, 0x98366C8E, 0x41102F83, 0xF60DEE87, 0xF35DA999, 0x4440689D, 0x9D662B90, 0x2A7BEA94,
    0xE71DB4E0, 0x500075E4, 0x892636E9, 0x3E3BF7ED, 0x3B6BB0F3, 0x8C7671F7, 0x555032FA, 0xE24DF3FE,
    0x5FF0BCC6, 0xE8ED7DC2, 0x31CB3ECF, 0x86D6FFCB, 0x8386B8D5, 0x349B79D1, 0xEDBD3ADC, 0x5AA0FBD8,
    0xEEE00C69, 0x59FDCD6D, 0x80DB8E60, 0x37C64F64, 0x3296087A, 0x858BC97E, 0x5CAD8A73, 0xEBB04B77,
    0x560D044F, 0xE110C54B, 0x38368646, 0x8F2B4742, 0x8A7B005C, 0x3D66C158, 0xE4408255, 0x535D4351,
    0x9E3B1D25, 0x2926DC21, 0xF0009F2C, 0x471D5E28, 0x424D1936, 0xF550D832, 0x2C769B3F, 0x9B6B5A3B,
    0x26D61503, 0x91CBD407, 0x48ED970A, 0xFFF0560E, 0xFAA01110, 0x4DBDD014, 0x949B9319, 0x2386521D,
    0x0E562FF1, 0xB94BEEF5, 0x606DADF8, 0xD7706CFC, 0xD2202BE2, 0x653DEAE6, 0xBC1BA9EB, 0x0B0668EF,
    0xB6BB27D7, 0x01A6E6D3, 0xD880A5DE, 0x6F9D64DA, 0x6ACD23C4, 0xDDD0E2C0, 0x04F6A1CD, 0xB3EB60C9,
    0x7E8D3EBD, 0xC990FFB9, 0x10B6BCB4, 0xA7AB7DB0, 0xA2FB3AAE, 0x15E6FBAA, 0xCCC0B8A7, 0x7BDD79A3,
    0xC660369B, 0x717DF79F, 0xA85BB492, 0x1F467596, 0x1A163288, 0xAD0BF38C, 0x742DB081, 0xC3307185,
    0x99908A5D, 0x2E8D4B59, 0xF7AB0854, 0x40B6C950, 0x45E68E4E, 0xF2FB4F4A, 0x2BDD0C47, 0x9CC0CD43,
    0x217D827B, 0x9660437F, 0x4F460072, 0xF85BC176, 0xFD0B8668, 0x4A16476C, 0x93300461, 0x242DC565,
    0xE94B9B11, 0x5E565A15, 0x87701918, 0x306DD81C, 0x353D9F02, 0x82205E06, 0x5B061D0B, 0xEC1BDC0F,
    0x51A69337, 0xE6BB5233, 0x3F9D113E, 0x8880D03A, 0x8DD09724, 0x3ACD5620, 0xE3EB152D, 0x54F6D429,
    0x7926A9C5, 0xCE3B68C1, 0x171D2BCC, 0xA000EAC8, 0xA550ADD6, 0x124D6CD2, 0xCB6B2FDF, 0x7C76EEDB,
    0xC1CBA1E3, 0x76D660E7, 0xAFF023EA, 0x18EDE2EE, 0x1DBDA5F0, 0xAAA064F4, 0x738627F9, 0xC49BE6FD,
    0x09FDB889, 0xBEE0798D, 0x67C63A80, 0xD0DBFB84, 0xD58BBC9A, 0x62967D9E, 0xBBB03E93, 0x0CADFF97,
    0xB110B0AF, 0x060D71AB, 0xDF2B32A6, 0x6836F3A2, 0x6D66B4BC, 0xDA7B75B8, 0x035D36B5, 0xB440F7B1,
    0x00000001,
    ];

fn check_crc32(data: &[u8], expected_crc: &[u8]) -> bool {
    use std::convert::TryInto;

    if expected_crc.len() < 4 {
        return false;
    }

    let mut crc: u32 = 0xFFFF_FFFF;

    for b in data {
        let i = crc as u8;
        crc = CRC32_TABLE[(i^b) as usize] ^ (crc >> 8);
    }

    let ecrc =  u32::from_be_bytes(expected_crc[0..4].try_into().unwrap());
    crc.swap_bytes() == ecrc
}

#[cfg(test)]
mod tests {
    use hex_literal::*;
    use super::*;

    #[test]
    fn parse_one_packet() {
        let packet: [u8; 188] = hex!("
        4740113e071003371a157e00000001e000008080052119b96e1f00000001
        0910000000016742e01eda02d0f6ffc00200024400002eec000afc801000
        00000168ce3c800000000165888000ea20bf87c142a09e07011de99660bb
        a665b11cb3d7ab982fec86863706aee6557731fb993b73bd78f1cce852bc
        da4c990883ee4942f1341feb3a6cbbfd5ea278781bcf1187f6549ea14162
        133d5dc5d455f8e23f0c75495a7a4bcf9df971537f5d7da266f502c74af9
        33695d2c62b155fd");

        let mut parser = Parser::new();
        let packet = parser.parse(&packet);
        //eprintln!("{:?}", packet);
        assert!(&packet.is_ok());
        let packet = packet.unwrap();
        assert_eq!(&packet.pid, &17);
        assert!(&packet.adaptation.is_some());
        assert!(!&packet.adaptation.unwrap().random_access_indicator);

        if let Some(Payload::Pes(pes)) = packet.payload {
            assert_eq!(pes.sid_type, SidType::Video);
        }
        else {
            assert!(false);
        }
    }

    #[test]
    fn parse_bad_sync() {
        let packet: [u8; 188] = hex!("
        5740113e071003371a157e00000001e000008080052119b96e1f00000001
        0910000000016742e01eda02d0f6ffc00200024400002eec000afc801000
        00000168ce3c800000000165888000ea20bf87c142a09e07011de99660bb
        a665b11cb3d7ab982fec86863706aee6557731fb993b73bd78f1cce852bc
        da4c990883ee4942f1341feb3a6cbbfd5ea278781bcf1187f6549ea14162
        133d5dc5d455f8e23f0c75495a7a4bcf9df971537f5d7da266f502c74af9
        33695d2c62b155fd");

        let mut parser = Parser::new();
        let packet = parser.parse(&packet);
        assert!(!&packet.is_ok());

        match packet {
            Err(TsParseError::BadSyncByte) => (),
            _ => assert!(false),
        }
    }

    #[test]
    fn parse_with_error_indicator() {
        let packet: [u8; 188] = hex!("
        47C0113e071003371a157e00000001e000008080052119b96e1f00000001
        0910000000016742e01eda02d0f6ffc00200024400002eec000afc801000
        00000168ce3c800000000165888000ea20bf87c142a09e07011de99660bb
        a665b11cb3d7ab982fec86863706aee6557731fb993b73bd78f1cce852bc
        da4c990883ee4942f1341feb3a6cbbfd5ea278781bcf1187f6549ea14162
        133d5dc5d455f8e23f0c75495a7a4bcf9df971537f5d7da266f502c74af9
        33695d2c62b155fd");

        let mut parser = Parser::new();
        let packet = parser.parse(&packet);
        eprintln!("{:?}", &packet);
        
        match packet {
            Err(TsParseError::ErrorIndicatorSet) => (),
            _ => assert!(false),
        }
    }

    #[test]
    fn parse_gop_start() {
        let packet: [u8; 940] = hex!("
       4740001b0000b00d0000c100000001e0102d50981bffffffffffffffffff
       ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
       ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
       ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
       ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
       ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
       ffffffffffffffff4740101b0002b0330001c10000e011f0001be011f006
       280442e01e3f15e021f01626090101ff4b4c5641000f2709c07800c00c00
       c0000031cb3e2dffffffffffffffffffffffffffffffffffffffffffffff
       ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
       ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
       ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
       ffffffffffffffffffffffffffffffff4740113f071003372c5d7e000000
       01e000008080052119b9b4cf00000001093000000001419a60fea483119b
       5cd24e9f5b9afa0682d31cb7ed765e70c051a1a3a4fd1667cd4efc260b07
       905c99c933ddf04207105442ca07f8efc6e0a1265145f933b5d89fad0300
       58420293761a266be7a124f4ac5da8efa5be9a6c12a9be6dda4d2f5b07a9
       566b6e245a851e967d05c0c18c8f2c453d6f90bb5536c236a7a71eb00ea2
       6467bae23579ae84811718eabe6b16516e73e9171a2c54b6470011100f19
       d4d7c3f83df4a8028425461c541a429be660e3ea9e94075232d721a292a0
       298d2c64e68f925f28afd2a64765d5f4146a3e15002011e48f83d6635954
       92436fa6d29d0ba01600348f192e369570c2df1b2ad925d039b511e778cd
       6fb5b47c761a6a9125a3479c188c96a10da86acbaaa7c6d70aa9fd5ca6e8
       0be0359462e0315854a9c70b19ce51ecf47a03c00a7718550a3d5d655529
       9271bd1fba0b6150a06468c5022d83eb879892d26721a60dad1d00ec07ee
       324d470011116aeb2ebcd425bb0b369b3e9bf28e617ddea7d7d66d22c18a
       51a81a11bb20452096165b66339ed0060a018506231e94556c8d7d6d503a
       027a718819bd165b68d3792fb59a19846e23ad5163b713b8866d03bdc3d2
       fa54737c562bfde9da46925251d18efa5f260f329bff8543713a67b64245
       a174e24b7c424b1af144eb9e930b9a88d442a6b2b4925a9ae1aef1fcddd4
       adc3d008a03b008d0152324b6e173ede8fab4974045024b431a0b1d88262
       954afda9bd4d3555525d  
       ");
        
        let mut parser = Parser::new();

        for chunk in packet.chunks_exact(188) {
            let pack = parser.parse(chunk);
            println!("TS: {:?}", pack);
        }

        assert!(parser.last_pat.is_some());
        //eprintln!("{:?}", &parser.last_pat.unwrap()[..]);
        //eprintln!("{:?}", &parser.last_pmt.unwrap()[..]);
        assert!(parser.last_pmt.is_some());
        assert!(parser.pmt_pid.is_some());
    }


    #[test]
    fn parse_gop_start_w_error() {
        let packet: [u8; 940] = hex!("
       4740001b0000b00d0000c100000001e1102d50981bffffffffffffffffff
       ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
       ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
       ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
       ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
       ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
       ffffffffffffffff4740101b0002b0330001c10000e011f0001be011f006
       280442e01e3f15e021f01626090101ff4b4c5641000f2709c07800c00c00
       c0000031cb3e2dffffffffffffffffffffffffffffffffffffffffffffff
       ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
       ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
       ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
       ffffffffffffffffffffffffffffffff4740113f071003372c5d7e000000
       01e000008080052119b9b4cf00000001093000000001419a60fea483119b
       5cd24e9f5b9afa0682d31cb7ed765e70c051a1a3a4fd1667cd4efc260b07
       905c99c933ddf04207105442ca07f8efc6e0a1265145f933b5d89fad0300
       58420293761a266be7a124f4ac5da8efa5be9a6c12a9be6dda4d2f5b07a9
       566b6e245a851e967d05c0c18c8f2c453d6f90bb5536c236a7a71eb00ea2
       6467bae23579ae84811718eabe6b16516e73e9171a2c54b6470011100f19
       d4d7c3f83df4a8028425461c541a429be660e3ea9e94075232d721a292a0
       298d2c64e68f925f28afd2a64765d5f4146a3e15002011e48f83d6635954
       92436fa6d29d0ba01600348f192e369570c2df1b2ad925d039b511e778cd
       6fb5b47c761a6a9125a3479c188c96a10da86acbaaa7c6d70aa9fd5ca6e8
       0be0359462e0315854a9c70b19ce51ecf47a03c00a7718550a3d5d655529
       9271bd1fba0b6150a06468c5022d83eb879892d26721a60dad1d00ec07ee
       324d470011116aeb2ebcd425bb0b369b3e9bf28e617ddea7d7d66d22c18a
       51a81a11bb20452096165b66339ed0060a018506231e94556c8d7d6d503a
       027a718819bd165b68d3792fb59a19846e23ad5163b713b8866d03bdc3d2
       fa54737c562bfde9da46925251d18efa5f260f329bff8543713a67b64245
       a174e24b7c424b1af144eb9e930b9a88d442a6b2b4925a9ae1aef1fcddd4
       adc3d008a03b008d0152324b6e173ede8fab4974045024b431a0b1d88262
       954afda9bd4d3555525d  
       ");
        
        let mut parser = Parser::new();

        for chunk in packet.chunks_exact(188) {
            let _ = parser.parse(chunk);
        }

        // bad CRC32 should fail parsing of the PAT
        assert!(parser.last_pat.is_none());
    }


    #[test]
    fn parse_with_adaptation() {
        let packet: [u8; 188] = hex!("
        47400030a600ffffffffffffffffffff
        ffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffff
        ffffffffffffffffffffffffffffffff
        ffffffffffffffffffffff0000b00d51
        e2f700000001e01000b63422");

        let mut parser = Parser::new();
        let res = parser.parse(&packet);

        assert!(res.is_ok());
        match res.unwrap().payload.unwrap() {
            Payload::Pat(_) => (),
            _ => assert!(false),
        }
    }
}

